<?php 
//**************************************************************************************//
//                      Fait par ANTOINE DURAND et THOMAS BOYER
//**************************************************************************************//
//----------------------------------------Params----------------------------------------------
require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');

$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    
    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

//----------------------------------------Terran----------------------------------------------
Flight::route('/units/@race', function($race){
    $data = array(
        'units' => null,
        'race' => $race,
    );
    if ($race == "terran") {
        $data['units'] = get_terran();
    }
    if ($race == "zerg") {
        $data['units'] = get_zerg();
    }  
    Flight::render('units.twig', $data);
});

Flight::route('/units/@race/@slug', function($race, $slug){
    $data = array(
        'unit' => null,
        'race' => $race,
    );
    if ($race == "terran") {
        $data['unit'] = get_terran_by_slug($slug);
    }
    if ($race == "zerg") {
        $data['unit'] = get_zerg_by_slug($slug);
    }  
    Flight::render('unit.twig', $data);
});

//----------------------------------------Home----------------------------------------------
Flight::route('/', function(){
    Flight::render('home.twig');
});


//----------------------------------------Start----------------------------------------------
Flight::start();